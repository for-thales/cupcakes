# Cupcake factory

Ecrire une code qui permet de créer des cupcakes avec autant de toppings que l'on souhaite comme : “Cupcake with chocolate and nuts” Or “Cupcake with black chocolate and peanuts and candy”

## Exercice:

- Créer une classe Topping:
    - Un topping a un nom et un prix
    - Un prix a une devise (euros, dollars, etc.) et un montant
    - Deux prix peuvent être sommés (`price1.plus(price2)` = `price3`) s'ils sont de la même devise


- Créer une classe Cake:
    - Un cake a un nom, un prix de base et une liste d'ingrédients
    - Un cake peut afficher sa recette : 'un cupcake avec du chocolat, du sucre et des noisettes'
    - Un cake peut afficher son prix
    - L'ordre des toppings est très importants

Une fois créé, un objet Cake ne peut plus être modifié.
On ne peut pas créer un Cake avec une liste d'ingrédients : on doit les passer un par un avec une méthode comme `cake.with(topping)` (la méthode with doit donc créer un nouveau Cake avec les ingrédients déjà présents et le nouveau topping)

> Penser à utiliser les différentes visibilités et constructeurs

- Créer une classe Bundle :
    - Un bundle contient une liste de cake
    - Le prix global d'un bundle est 10% moins cher
    - Un bundle peut contenir un seul Cake


- Créer une Cupcake Factory :
    - Permet de créer des cakes à partir d'une liste de recette

Les cakes peuvent être vendus à condition de ne pas être périmés. Un cake se périme au bout de 2 jours et peut être vendu dans un bundle 12h avant sa date de péremption.


## Tests

### About name function or method

    The name function should return “Cupcake”
    The name function should return “Cookie”
    The name function should return “Cupcake with chocolate”
    The name function should return “Cookie with chocolate”
    The name function should return “Cookie with chocolate and peanuts”
    The name function should return “Cookie with peanuts and chocolate”

### About price function or method

    The price function should return 1$ for “Cupcake”
    The price function should return 2$ for “Cookie”
    The price function should return 1.1$ for “Cupcake with chocolate”
    The price function should return 2.1$ for “Cookie with chocolate”
    The price function should return 2.2$ for “Cookie with peanuts”

### Bundle

    We can build a Bundle with 1 Cupcake and check price or description
    We can build a Bundle with 1 Cupcake and 1 Cookie and check price or description
    We can build a Bundle with 2 Cupcake and 1 Cookie and check price or description
    We can build a bundle with 1 bundle of 2 cakes and 1 Cupcake and check price or description
    We can build a bundle with many bundle of bundle and many cakes and check price or description